package ru.amprograms.iqs.user.services;

import java.util.Set;

import ru.amprograms.iqs.user.types.IqsRole;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * Methods for working with user information<br>
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsUserService {

    /**
     * Find user by login
     * @param login
     * @return the user
     */
    IqsUser findByLogin(String login);

    /**
     * Return roles for the given user
     * @param userId
     * @return set of roles
     */
    Set<? extends IqsRole> getUserRoles(Long userId);

    /**
     * @return all users
     */
    Set<? extends IqsUser> getAllUsers();

    /**
     * @return enabled users
     */
    Set<? extends IqsUser> getEnabledUsers();
}
