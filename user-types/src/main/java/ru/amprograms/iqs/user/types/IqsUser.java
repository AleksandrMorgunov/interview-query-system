package ru.amprograms.iqs.user.types;

/**
 * User<br>
 * 
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsUser {

	/**
	 * @return the user's ID
	 */
	Long getId();
	/**
	 * @return the user's login
	 */
	String getLogin();
	/**
	 * @return the user's password
	 */
	String getPassword();
	/**
	 * @return the user's name
	 */
	String getName();
	/**
	 * @return if the user is enabled
	 */
	Boolean getEnabled();

}