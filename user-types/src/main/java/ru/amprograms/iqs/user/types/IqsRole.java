package ru.amprograms.iqs.user.types;

/**
 * The role<br>
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsRole {
	
	String getCode();
	
	String getName();
	
}
