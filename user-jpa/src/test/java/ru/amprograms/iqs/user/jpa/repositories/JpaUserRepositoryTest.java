package ru.amprograms.iqs.user.jpa.repositories;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ru.amprograms.iqs.user.jpa.JpaTestConfig;
import ru.amprograms.iqs.user.jpa.types.JpaRole;
import ru.amprograms.iqs.user.jpa.types.JpaUser;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = JpaTestConfig.class)
public class JpaUserRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private JpaUserRepository userRepo;
    @Autowired
    private JpaRoleRepository roleRepo;

    private static final String USERNAME = "UserName";
    private JpaUser savedUser;

    private JpaRole roleUser;

    @BeforeClass
    public void initiate() {
        try {
            JpaUser user = userRepo.findByLogin(USERNAME);
            userRepo.delete(user);
        } catch (Throwable e) {

        }
        roleUser = roleRepo.findOne(1L);
    }

    @AfterClass
    public void cleanUp() {
        try {
            JpaUser user = userRepo.findByLogin(USERNAME);
            userRepo.delete(user);
        } catch (Throwable e) {

        }
    }

    @Test
    public void create() {
        JpaUser user = new JpaUser();
        user.setLogin(USERNAME);
        user.setPassword("password");
        user.setEnabled(true);
        user.setName("name");

        Set<JpaRole> roleSet = new HashSet<>();
        roleSet.add(roleUser);

        Assert.assertNotNull(userRepo);
        savedUser = userRepo.save(user);
        Assert.assertNotNull(savedUser);
        Assert.assertNotNull(savedUser.getId());
    }

    @Test(dependsOnMethods = "create")
    public void findByLogin() {
        JpaUser user = userRepo.findByLogin(USERNAME);
        Assert.assertNotNull(user);
        Assert.assertEquals(user, savedUser);
    }

    @Test(
            dependsOnMethods = "create",
            expectedExceptions = org.springframework.dao.DataIntegrityViolationException.class)
    public void createDuplicate() {
        JpaUser user = new JpaUser();
        user.setLogin(USERNAME);
        user.setPassword("password");
        user.setEnabled(true);
        user.setName("name");

        Assert.assertNotNull(userRepo);
        savedUser = userRepo.save(user);
        Assert.assertNotNull(savedUser);
        Assert.assertNotNull(savedUser.getId());
    }

}
