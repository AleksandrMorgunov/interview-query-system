package ru.amprograms.iqs.user.jpa.repositories;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.user.jpa.types.JpaUser;

public interface JpaUserRepository extends CrudRepository<JpaUser, Long> {

    public JpaUser findByLogin(String login);

    @Override
    public Set<JpaUser> findAll();

    public Set<JpaUser> findByEnabled(Boolean enabled);
}
