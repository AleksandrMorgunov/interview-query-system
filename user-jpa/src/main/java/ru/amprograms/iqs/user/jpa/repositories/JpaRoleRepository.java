package ru.amprograms.iqs.user.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.user.jpa.types.JpaRole;

public interface JpaRoleRepository extends CrudRepository<JpaRole, Long> {

}
