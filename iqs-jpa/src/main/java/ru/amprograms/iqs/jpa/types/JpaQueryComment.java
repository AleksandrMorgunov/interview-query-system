package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsQuestionComment;
import ru.amprograms.iqs.user.types.IqsUser;


@Entity
@Table(name = "iqs_query_comment")
public class JpaQueryComment implements Serializable, IqsQuestionComment {

	private static final long serialVersionUID = -8771228503882253325L;

	//TODO add id generator
	@Id
	@Column(name="comment_id")
	private Long id;

	//TODO Describe the link
	private JpaQuery query;

	//TODO Describe as LOB
	private String text;

	@Column(name = "author_id")
	private Long authorId;

	@Column(name="create_time")
	private Instant createTime;

	public JpaQueryComment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getAuthorId() {
		return authorId;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryComment#getCreateTime()
	 */
	@Override
	public Instant getCreateTime() {
		return createTime;
	}


	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryComment#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}


	public JpaQuery getQuery() {
		return query;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryComment#getText()
	 */
	@Override
	public String getText() {
		return text;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setCreateTime(Instant createTime) {
		this.createTime = createTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setQuery(JpaQuery query) {
		this.query = query;
	}

	public void setText(String text) {
		this.text = text;
	}


	@Override
	public IqsUser getAuthor() {
		throw new UnsupportedOperationException();
	}

}
