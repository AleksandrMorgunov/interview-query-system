package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQuery;

public interface JpaQueryRepository extends CrudRepository<JpaQuery, Long> {

}
