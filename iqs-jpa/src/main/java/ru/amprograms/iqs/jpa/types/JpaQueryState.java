package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsQuestionState;
import ru.amprograms.iqs.user.types.IqsUser;

@Entity
@Table(name="iqs_query_state")
public class JpaQueryState implements Serializable, IqsQuestionState {

	private static final long serialVersionUID = 594683083325433909L;

	@Id
	@Column(name="query_state_id")
	private Long id;

	private JpaQuery query;

	@Column(name = "text", nullable = false, length=1000)
	private String text;

	//TODO describe as LOB 
	private String answer;

	//TODO describe the link
	private JpaCategory category;

	@Column(name="time_for_answer")
	private Integer timeForAnswer;

	@Column(name="is_draft")
	private Boolean draft;

	@Column(name="avg_mark")
	private Float avgMark;

	@Column(name = "author_id")
	private Long authorId;

	@Column(name="create_time")
	private Instant createTime;

	/**
	 * One second before the createTime for the next version
	 */
	@Column(name="ent_time")
	private Instant endTime;

	public JpaQueryState() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getAnswer() {
		return answer;
	}

	public Long getAuthorId() {
		return authorId;
	}

	@Override
	public Float getAvgMark() {
		return avgMark;
	}

	@Override
	public JpaCategory getCategory() {
		return category;
	}

	@Override
	public Instant getCreateTime() {
		return createTime;
	}

	@Override
	public Boolean getDraft() {
		return draft;
	}

	public Instant getEndTime() {
		return endTime;
	}

	@Override
	public Long getId() {
		return id;
	}

	public JpaQuery getQuery() {
		return query;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public Integer getTimeForAnswer() {
		return timeForAnswer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setAvgMark(Float avgMark) {
		this.avgMark = avgMark;
	}

	public void setCategory(JpaCategory category) {
		this.category = category;
	}

	public void setCreateTime(Instant createTime) {
		this.createTime = createTime;
	}

	public void setDraft(Boolean draft) {
		this.draft = draft;
	}

	public void setEndTime(Instant endTime) {
		this.endTime = endTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setQuery(JpaQuery query) {
		this.query = query;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setTimeForAnswer(Integer timeForAnswer) {
		this.timeForAnswer = timeForAnswer;
	}

	@Override
	public Long getItemId() {
		return query.getId();
	}

	@Override
	public IqsUser getAuthor() {
		// TODO Auto-generated method stub
		return null;
	}

}
