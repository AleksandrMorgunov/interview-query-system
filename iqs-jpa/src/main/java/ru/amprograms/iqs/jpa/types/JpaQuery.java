package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsQuestion;

@Entity
@Table(name = "iqs_query")
public class JpaQuery implements Serializable, IqsQuestion {

	private static final long serialVersionUID = 1416831533589533157L;

	@Id
	@Column(name="query_id")
	private Long id;

	//TODO Describe the link
	private JpaQueryState currentState;

	//TODO Describe the link
	private List<JpaQueryState> history;

	//TODO Describe the link
	private Set<JpaQueryComment> comments;

	//TODO Describe the link
	private Set<JpaQueryMark> marks;

	private Set<JpaQueryPosition> positions;

	private Set<JpaQueryResorceUrl> urls;

	public JpaQuery() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Set<JpaQueryComment> getComments() {
		return comments;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQuery#getCurrentState()
	 */
	@Override
	public JpaQueryState getCurrentState() {
		return currentState;
	}

	@Override
	public List<JpaQueryState> getHistory() {
		return history;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQuery#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public Set<JpaQueryMark> getMarks() {
		return marks;
	}

	@Override
	public Set<JpaQueryPosition> getPositions() {
		return positions;
	}

	@Override
	public Set<JpaQueryResorceUrl> getUrls() {
		return urls;
	}

	public void setComments(Set<JpaQueryComment> comments) {
		this.comments = comments;
	}

	public void setCurrentState(JpaQueryState currentState) {
		this.currentState = currentState;
	}

	public void setHistory(List<JpaQueryState> history) {
		this.history = history;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMarks(Set<JpaQueryMark> marks) {
		this.marks = marks;
	}

	public void setPositions(Set<JpaQueryPosition> positions) {
		this.positions = positions;
	}

	public void setUrls(Set<JpaQueryResorceUrl> urls) {
		this.urls = urls;
	}

}
