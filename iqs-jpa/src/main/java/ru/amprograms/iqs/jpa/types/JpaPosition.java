package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsPosition;

@Entity
@Table(name="iqs_position")
public class JpaPosition implements Serializable, IqsPosition {

	private static final long serialVersionUID = 3872042864264675895L;

	//TODO describe id generator
	@Id
	@Column(name = "position_id")
	private Long id;
	
	@Column(name = "name", nullable = false, length=50)
	private String name;
	
	//TODO describe the link
	private JpaPosition parentPosition;

	public JpaPosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsPosition#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsPosition#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public JpaPosition getParentPosition() {
		return parentPosition;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParentPosition(JpaPosition parentPosition) {
		this.parentPosition = parentPosition;
	}

	@Override
	public Long getParentPositionId() {
		return parentPosition != null ? parentPosition.getId() : null;
	}
	
	
}
