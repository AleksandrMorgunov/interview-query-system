package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQueryPosition;

public interface JpaQueryPositionRepository extends CrudRepository<JpaQueryPosition, Long> {

}
