package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaPosition;

public interface JpaPositionRepository extends CrudRepository<JpaPosition, Long> {

}
