package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQueryComment;

public interface JpaQueryCommentRepository extends CrudRepository<JpaQueryComment, Long> {

}
