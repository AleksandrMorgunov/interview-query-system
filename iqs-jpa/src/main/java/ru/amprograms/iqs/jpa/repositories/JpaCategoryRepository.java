package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaCategory;

public interface JpaCategoryRepository extends CrudRepository<JpaCategory, Long> {

}
