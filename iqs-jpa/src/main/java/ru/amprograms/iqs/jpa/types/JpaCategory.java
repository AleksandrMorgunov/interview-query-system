package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsCategory;

@Entity
@Table(name = "iqs_category")
public class JpaCategory implements Serializable, IqsCategory {

	
	private static final long serialVersionUID = 1766439583888616033L;
	
	//TODO add id generator
	@Id
	@Column(name = "category_id")
	private Long id;
	
	@Column(name = "name", nullable = false, length=50)
	private String name;
	
	//TODO describe the link
	@Column(name = "parent_category")
	private JpaCategory parentCategory;

	public JpaCategory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsCategory#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	public JpaCategory getParentCategory() {
		return parentCategory;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParentCategory(JpaCategory parentCategory) {
		this.parentCategory = parentCategory;
	}

	@Override
	public Long getParentCategoryId() {		
		return parentCategory != null ? parentCategory.getId() : null;
	}
	
	
}
