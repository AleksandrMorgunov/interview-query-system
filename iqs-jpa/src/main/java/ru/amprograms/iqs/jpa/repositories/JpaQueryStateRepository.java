package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQueryState;

public interface JpaQueryStateRepository extends CrudRepository<JpaQueryState, Long> {

}
