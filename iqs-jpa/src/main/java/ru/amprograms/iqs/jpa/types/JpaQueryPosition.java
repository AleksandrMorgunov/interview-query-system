package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsQuestionPosition;
import ru.amprograms.iqs.user.types.IqsUser;

@Entity
@Table(name="iqs_query_position")
public class JpaQueryPosition implements Serializable, IqsQuestionPosition {

	private static final long serialVersionUID = 3872042864264675895L;

	//TODO add id generator
	@Id
	@Column(name="query_position_id", nullable=false)
	private Long id;

	private JpaQuery query;

	private JpaPosition position;

	@Column(name="is_deleted")
	private Boolean deleted;

	/**
	 * The user who add the position to the query 
	 */
	@Column(name = "author_id")
	private Long authorId;

	@Column(name="create_time")
	private Instant createTime;

	@Column(name="end_time")
	private Instant endTime;

	public JpaQueryPosition() {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryPosition#getCreateTime()
	 */
	@Override
	public Instant getCreateTime() {
		return createTime;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Instant getEndTime() {
		return endTime;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryPosition#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryPosition#getPosition()
	 */
	@Override
	public JpaPosition getPosition() {
		return position;
	}

	public JpaQuery getQuery() {
		return query;
	}

	public void setCreateTime(Instant createTime) {
		this.createTime = createTime;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setEndTime(Instant endTime) {
		this.endTime = endTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPosition(JpaPosition position) {
		this.position = position;
	}

	public void setQuery(JpaQuery query) {
		this.query = query;
	}

	@Override
	public IqsUser getAuthor() {
		throw new UnsupportedOperationException();
	}

}
