package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQueryResorceUrl;

public interface JpaQueryResorceUrlRepository extends CrudRepository<JpaQueryResorceUrl, Long> {

}
