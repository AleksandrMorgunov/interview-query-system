package ru.amprograms.iqs.jpa.repositories;

import org.springframework.data.repository.CrudRepository;

import ru.amprograms.iqs.jpa.types.JpaQueryMark;

public interface JpaQueryMarkRepository extends CrudRepository<JpaQueryMark, Long> {

}
