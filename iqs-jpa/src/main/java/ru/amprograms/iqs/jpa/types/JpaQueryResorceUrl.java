package ru.amprograms.iqs.jpa.types;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ru.amprograms.iqs.types.IqsQuestionResourceUrl;
import ru.amprograms.iqs.user.types.IqsUser;

@Entity
@Table(name="iqs_query_resorce_url")
public class JpaQueryResorceUrl implements Serializable, IqsQuestionResourceUrl {

	private static final long serialVersionUID = -3173279490662192476L;


	@Id
	@Column(name="query_resource_url_id")
	private Long id;

	//TODO Describe the link
	private JpaQuery query;

	@Column(name="url_value", nullable=false, length=3000)
	private String value;

	//TODO Describe as LOB
	private String comment;

	@Column(name="is_deleted")
	private Boolean deleted;

	@Column(name = "author_id")
	private Long authorId;

	@Column(name="create_time")
	private Instant createTime;

	@Column(name="end_time")
	private Instant endTime;

	public JpaQueryResorceUrl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getAuthorId() {
		return authorId;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryResourceUrl#getComment()
	 */
	@Override
	public String getComment() {
		return comment;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryResourceUrl#getCreateTime()
	 */
	@Override
	public Instant getCreateTime() {
		return createTime;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public Instant getEndTime() {
		return endTime;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryResourceUrl#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	public JpaQuery getQuery() {
		return query;
	}

	/* (non-Javadoc)
	 * @see ru.amprograms.iqs.jpa.types.IqsQueryResourceUrl#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setCreateTime(Instant createTime) {
		this.createTime = createTime;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void setEndTime(Instant endTime) {
		this.endTime = endTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setQuery(JpaQuery query) {
		this.query = query;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public IqsUser getAuthor() {
		throw new UnsupportedOperationException();
	}

}
