package ru.amprograms.iqs.jsf.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import ru.amprograms.iqs.user.services.IqsUserService;
import ru.amprograms.iqs.user.types.IqsRole;
import ru.amprograms.iqs.user.types.IqsUser;

@Component("iqsAuthenticationProvider")
public class IqsAuthenticationProvider implements AuthenticationProvider {

	@Inject
	protected IqsUserService userService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String login = authentication.getName();
		String password = authentication.getCredentials().toString();

		IqsUser user = userService.findByLogin(login);

		if (user != null && BCrypt.checkpw(password, user.getPassword())
				&& user.getEnabled() != null && user.getEnabled()) {
			List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
			
			Set<IqsRole> roles = userService.getUserRoles(user.getId());
			for (IqsRole role : roles)
				grantedAuths.add(new SimpleGrantedAuthority(role.getCode()));
			Authentication auth = new UsernamePasswordAuthenticationToken(user,
					password, grantedAuths);
			return auth;
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication);
	}

}
