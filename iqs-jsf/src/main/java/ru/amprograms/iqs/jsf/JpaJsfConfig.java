package ru.amprograms.iqs.jsf;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;

import ru.amprograms.iqs.user.jpa.repositories.JpaUserRepository;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = JpaUserRepository.class)
public class JpaJsfConfig {

	@Value("${dataSource.driverClassName}")
    private String driver;
    @Value("${dataSource.jndi.name}")
    private String jndiName;
    @Value("${hibernate.dialect}")
    private String dialect;
    @Value("${hibernate.hbm2ddl.auto}")
    private String hbm2ddlAuto;
    @Value("${hibernate.show_sql}")
	private String show_sql;
    @Value("${hibernate.format_sql}")
	private String format_sql;
    
	@Bean
    public AbstractPlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }
	
	 @Bean
	    public EntityManagerFactory entityManagerFactory() {
		 LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
	        entityManagerFactoryBean.setDataSource(dataSource());
	        entityManagerFactoryBean.setPackagesToScan("ru.amprograms.iqs.jpa.types");
	        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

	        Properties jpaProperties = new Properties();
	        jpaProperties.put(org.hibernate.cfg.Environment.DIALECT, dialect);
	        jpaProperties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, hbm2ddlAuto);
	        
	        jpaProperties.put(org.hibernate.cfg.Environment.SHOW_SQL, show_sql);
	        jpaProperties.put(org.hibernate.cfg.Environment.FORMAT_SQL, format_sql);
	        
	        entityManagerFactoryBean.setJpaProperties(jpaProperties);

	        return entityManagerFactoryBean.getObject();
	    }

	    @Bean
	    public DataSource dataSource() {
	        final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
	        dsLookup.setResourceRef(true);
	        DataSource dataSource = dsLookup.getDataSource(jndiName);
	        return dataSource;
	    }
}
