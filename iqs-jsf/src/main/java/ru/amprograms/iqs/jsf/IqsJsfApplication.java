package ru.amprograms.iqs.jsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IqsJsfApplication {

	public static void main(String[] args) {
		SpringApplication.run(IqsJsfApplication.class, args);
	}
}
