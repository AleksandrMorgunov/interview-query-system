package ru.amprograms.iqs.types;

/**
 * A job position<br>
 * The question can be asked of the candidates for current and higher positions.
 * <br>
 * <br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsPosition {
	/**
	 * @return the ID of the position
	 */
	Long getId();
	/**
	 * @return the name of the position
	 */
	String getName();
	/**
	 * @return the parent position ID
	 */
	Long getParentPositionId();

}