package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;
/**
 * The position for the current question.<br>
 * The question can be asked of the candidates for current and higher positions<br>
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionPosition {
	/**
	 * @return ID of the current record
	 */
	Long getId();
	/**
	 * @return position
	 */
	IqsPosition getPosition();
	/**
	 * @return the server time when this position was added to the query
	 */
	Instant getCreateTime();
	/**
	 * @return user who added this position to the query
	 */
	IqsUser getAuthor();

}