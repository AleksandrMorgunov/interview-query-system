package ru.amprograms.iqs.types;

import java.util.List;

/**
 * The task<br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsTask extends IqsItem {

	/**
	 * @return the current state
	 */
	IqsTaskState getCurrentState();

	/**
	 * 
	 * @return arranged in chronological order a list of states of the question
	 */
	List<? extends IqsTaskState> getHistory();
}
