package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The comment to the question<br>
 * <br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionComment {
	/**
	 * @return the query comment ID
	 */
	Long getId();
	/**
	 * @return the query comment text
	 */
	String getText();
	/**
	 * @return the query comment author
	 */
	IqsUser getAuthor();
	/**
	 * @return the creation time of the query comment
	 */
	Instant getCreateTime();
}