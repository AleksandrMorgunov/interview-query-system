package ru.amprograms.iqs.types;

import java.util.List;

/**
 * The question<br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestion extends IqsItem {

	/**
	 * @return the current state of the question
	 */
	IqsQuestionState getCurrentState();

	/**
	 * @return the ordered list of question states
	 */
	List<? extends IqsQuestionState> getHistory();

}