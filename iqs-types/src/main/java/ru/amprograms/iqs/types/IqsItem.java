package ru.amprograms.iqs.types;

import java.util.Set;

/**
 * The parent interface for questions and tasks<br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsItem {

	/**
	 * @return the ID
	 */
	Long getId();

	/**
	 * @return the comments
	 */
	Set<? extends IqsQuestionComment> getComments();

	/**
	 * @return the marks
	 */
	Set<? extends IqsQuestionMark> getMarks();

	/**
	 * @return the job positions
	 */
	Set<? extends IqsQuestionPosition> getPositions();

	/**
	 * @return the resource' urls
	 */
	Set<? extends IqsQuestionResourceUrl> getUrls();

}