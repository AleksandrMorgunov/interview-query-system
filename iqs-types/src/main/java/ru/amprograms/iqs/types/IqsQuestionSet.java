package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The set of questions and tasks<br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionSet {

	/**
	 * @return the question set ID
	 */
	Long getId();

	/**
	 * @return the question set name
	 */
	String getName();

	/**
	 * @return time for answer, minutes
	 */
	Integer getTimeForAnswer();

	/**
	 * @return the author of the last changes
	 */
	IqsUser getAuthor();

	/**
	 * @return the owner of the set
	 */
	IqsUser getOwner();

	/**
	 * @return if this question set is available for all users
	 */
	Boolean isAvailableToAll();

	/**
	 * @return the time of the creation or the last update
	 */
	Instant getUpdateTime();
}
