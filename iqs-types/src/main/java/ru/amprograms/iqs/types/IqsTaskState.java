package ru.amprograms.iqs.types;

/**
 * The version of a task
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsTaskState extends IqsQuestionState {

	/**
	 * @return the text of the task
	 */
	String getTask();
}
