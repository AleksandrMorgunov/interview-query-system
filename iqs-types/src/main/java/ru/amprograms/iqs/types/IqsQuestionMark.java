package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The mark to the question
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionMark {
	/**
	 * @return the query mark ID
	 */
	Long getId();
	/**
	 * @return the query mark value
	 */
	Integer getValue();
	/**
	 * @return the comment to the query mark
	 */
	String getComment();
	/**
	 * @return the author of the query mark
	 */
	IqsUser getAuthor();
	/**
	 * @return the creation tome of the query mark
	 */
	Instant getCreateTime();
}