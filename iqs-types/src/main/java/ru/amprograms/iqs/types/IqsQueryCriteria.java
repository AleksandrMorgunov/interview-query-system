package ru.amprograms.iqs.types;

import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import ru.amprograms.iqs.user.types.IqsUser;



public interface IqsQueryCriteria {
	public enum ItemType {
		QUESTION, TASK, ALL
	};

	public enum IsDraft {
		TRUE, FALSE, ALL
	};

	/**
	 * 
	 * @return type of item: question, task or both
	 */
	ItemType getItemType();

	/**
	 * All words should be present in the text of question
	 * 
	 * @return
	 */
	Set<String> getTextWords();

	/**
	 * All words should be present in the answer
	 * 
	 * @return
	 */
	Set<String> getAnswerWords();

	/**
	 * All words should be present in the task description
	 * 
	 * @return
	 */
	Set<String> getTaskWords();

	/**
	 * Query should return items from given categories. If the second parameter
	 * is true, then query should return items from subcategory.
	 * 
	 * @return
	 */
	Set<Pair<IqsCategory, Boolean>> getCategories();

	/**
	 * Query should return items for given positions. If the second parameter is
	 * true, then query should return items for subordinate positions.
	 * 
	 * @return
	 */
	Set<Pair<IqsPosition, Boolean>> getPositions();

	/**
	 * Query should return items with time for answer more or equal than given
	 * value
	 * 
	 * @return
	 */
	Integer getMinTimeForAnswer();

	/**
	 * Query should return items with time for answer less or equal than given
	 * value
	 * 
	 * @return
	 */
	Integer getMaxTimeForAnswer();

	/**
	 * Query should return items with mark more or equal than given value
	 * 
	 * @return
	 */
	Float getMinMark();

	/**
	 * Query should return items with mark less or equal than given value
	 * 
	 * @return
	 */
	Float getMaxMark();

	/**
	 * Query should return items with given author
	 * 
	 * @return
	 */
	IqsUser getAuthor();

	/**
	 * Specify which items should return query: draft, non-draft or both
	 * 
	 * @return
	 */
	IsDraft isDraft();

	/**
	 * If this value is not null, than query should return items only from given
	 * set
	 * 
	 * @return
	 */
	IqsQuestionSet getQuestionSet();

}
