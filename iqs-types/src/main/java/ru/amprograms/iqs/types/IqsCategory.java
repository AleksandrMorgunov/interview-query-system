package ru.amprograms.iqs.types;

/**
 * The category of a query<br>
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsCategory {

	/**
	 * @return the category ID
	 */
	Long getId();
	/**
	 * @return the category name
	 */
	String getName();
	/**
	 * @return the parent category ID
	 */
	Long getParentCategoryId();

}