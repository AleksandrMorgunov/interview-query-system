package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;
/**
 * A link to a website with information relevant to the question<br>
 * <br>
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionResourceUrl {
	/**
	 * @return the ID of the link
	 */
	Long getId();
	/**
	 * @return the value of the link
	 */
	String getValue();
	/**
	 * @return the comment to the link
	 */
	String getComment();
	/**
	 * @return the user, who created the link
	 */
	IqsUser getAuthor();
	/**
	 * @return the time when the link was been created
	 */
	Instant getCreateTime();


}