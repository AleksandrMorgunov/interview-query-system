package ru.amprograms.iqs.types;

import java.time.Instant;

import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The version of the question.<br>
 * <br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionState {

	/**
	 * @return the question/task ID
	 */
	Long getItemId();

	/**
	 * @return question/task state ID
	 */
	Long getId();
	
	/**
	 * @return question/task text
	 */
	String getText();	
	
	/**
	 * @return the answer for the question/task
	 */
	String getAnswer();
	
	/**
	 * @return the category of the question/task
	 */
	IqsCategory getCategory();
	/**
	 * @return time for answer, minutes
	 */
	Integer getTimeForAnswer();
	
	/**
	 * @return the author of the current edition of the question/task
	 */
	IqsUser getAuthor();
	
	/**
	 * @return the average mark of the question/task
	 */
	Float getAvgMark();
	
	/**
	 * @return shows if this question/task is a draft
	 */
	Boolean getDraft();
	/**
	 * @return the time of the creation of the current version
	 */
	Instant getCreateTime();

}