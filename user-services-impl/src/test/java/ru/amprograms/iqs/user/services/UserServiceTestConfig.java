package ru.amprograms.iqs.user.services;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ru.amprograms.iqs.user.jpa.JpaTestConfig;

@Configuration
@ComponentScan("ru.amprograms.iqs.user.services")
@Import(JpaTestConfig.class)
public class UserServiceTestConfig {


}
