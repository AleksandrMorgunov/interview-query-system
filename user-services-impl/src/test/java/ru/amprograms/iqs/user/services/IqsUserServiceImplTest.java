package ru.amprograms.iqs.user.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import ru.amprograms.iqs.user.jpa.repositories.JpaRoleRepository;
import ru.amprograms.iqs.user.jpa.repositories.JpaUserRepository;
import ru.amprograms.iqs.user.jpa.types.JpaRole;
import ru.amprograms.iqs.user.jpa.types.JpaUser;

//@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(classes = UserServiceTestConfig.class)
public class IqsUserServiceImplTest extends AbstractTestNGSpringContextTests {

    private static final String USERNAME = "UserName";

    @Autowired
    private IqsUserServiceImpl service;
    @Autowired
    private JpaUserRepository userRepo;
    @Autowired
    private JpaRoleRepository roleRepo;

    private boolean cleanupNeeded = false;

    private JpaUser user;

    private JpaRole roleUser;

    @BeforeClass
    public void beforeClass() {

        user = userRepo.findByLogin(USERNAME);

        if (user == null) {
            roleUser = roleRepo.findOne(1L);

            user = new JpaUser();
            user.setLogin(USERNAME);
            user.setPassword("password");
            user.setEnabled(true);
            user.setName("name");

            Set<JpaRole> roleSet = new HashSet<JpaRole>();
            roleSet.add(roleUser);

            Assert.assertNotNull(userRepo);
            user = userRepo.save(user);

            cleanupNeeded = true;
        }
    }

    @AfterClass
    public void afterClass() {
        if (cleanupNeeded) {
            JpaUser user = userRepo.findByLogin(USERNAME);
            userRepo.delete(user);
        }
    }


    @Test
    public void findByLogin() {
        JpaUser user = userRepo.findByLogin(USERNAME);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), USERNAME);
    }

    @Test
    public void getAllUsers() {
        Set<JpaUser> set = service.getAllUsers();
        Assert.assertNotNull(set);
        Assert.assertTrue(set.contains(user));
    }

    @Test
    public void getEnabledUsers() {
        Set<JpaUser> set = service.getAllUsers();
        Assert.assertNotNull(set);
        Assert.assertTrue(set.contains(user));
    }

    @Test
    public void getUserRoles() {
        Set<JpaRole> roles = service.getUserRoles(user.getId());
        Assert.assertNotNull(roles);
        Assert.assertTrue(roles.contains(roleUser));
    }
}
