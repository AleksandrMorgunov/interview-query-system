package ru.amprograms.iqs.user.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.amprograms.iqs.user.jpa.repositories.JpaUserRepository;
import ru.amprograms.iqs.user.jpa.types.JpaRole;
import ru.amprograms.iqs.user.jpa.types.JpaUser;
import ru.amprograms.iqs.user.types.IqsUser;

@Service
public class IqsUserServiceImpl implements IqsUserService {

    @Autowired
    private JpaUserRepository userRepository;

    public IqsUser findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public Set<JpaRole> getUserRoles(Long userId) {
        JpaUser user = userRepository.findOne(userId);
        if (user != null) {
            return user.getRoles();
        }
        return null;
    }

    public Set<JpaUser> getAllUsers() {
        return userRepository.findAll();
    }

    public Set<JpaUser> getEnabledUsers() {
        return userRepository.findByEnabled(true);
    }

}
