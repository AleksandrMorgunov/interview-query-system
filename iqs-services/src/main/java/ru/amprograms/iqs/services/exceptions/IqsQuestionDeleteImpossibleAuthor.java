package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * It is impossible to delete the question, because
 * the question was created by another author<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDeleteImpossibleAuthor extends Exception {

	private static final long serialVersionUID = 4117467034957848017L;
	
	private @Getter IqsUser user;
}
