package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsQuestionSet;

/**
 * It is impossible to delete the question, because
 * the question is present in another set<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDeleteImpossibleQuestionSet extends Exception {

	private static final long serialVersionUID = -3783251193984790818L;
	/**
	 * the list of the sets which are included the question
	 */
	private @Getter Set<IqsQuestionSet> questionSets;
}
