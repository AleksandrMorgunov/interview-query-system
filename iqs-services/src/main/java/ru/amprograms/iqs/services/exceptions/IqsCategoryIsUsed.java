package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsCategory;

/**
 * The category is used in a query
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsCategoryIsUsed extends Exception {

	private static final long serialVersionUID = 5496586524559828823L;

	private @Getter IqsCategory currentCategory;
}
