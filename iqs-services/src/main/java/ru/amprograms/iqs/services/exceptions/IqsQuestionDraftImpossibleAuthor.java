package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * It is impossible to set the draft attribute of the question to true, because
 * the question was created by another author<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDraftImpossibleAuthor extends Exception {

	private static final long serialVersionUID = 5181877941507473963L;
	
	private @Getter IqsUser user;
}
