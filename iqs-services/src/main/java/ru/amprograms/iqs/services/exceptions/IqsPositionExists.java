package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsPosition;

/**
 * A position with the same name already exists in the system
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsPositionExists extends Exception {

	private static final long serialVersionUID = -3957062113845894087L;

	/**
	 * The job position, which already exists in the system
	 */
	@Getter
	private IqsPosition existingPosition;

	/**
	 * new or changed position
	 */
	@Getter
	private IqsPosition currentPosition;

	
}
