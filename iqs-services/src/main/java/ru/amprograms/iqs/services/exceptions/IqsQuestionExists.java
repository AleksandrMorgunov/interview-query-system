package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsQuestion;

/**
 * The question with the same text exists in the system<br>
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionExists extends Exception {
	
	/**
	 * The new text for the question
	 */
	private @Getter String newQueryText;
	/**
	 * The question with the same text
	 */
	private @Getter IqsQuestion existingQuestion;

	
}
