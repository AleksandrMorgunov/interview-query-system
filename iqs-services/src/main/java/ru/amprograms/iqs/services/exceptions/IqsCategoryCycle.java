package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsCategory;

/**
 * Change the parent category leads to the cycle of categories
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsCategoryCycle extends Exception {

	
	private @Getter Set<IqsCategory> categoryCycle;
}
