package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsPosition;

/**
 * The job position is used in a query
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsPositionIsUsed extends Exception {

	private static final long serialVersionUID = 1403396904115988711L;
	
	@Getter private IqsPosition position;
}
