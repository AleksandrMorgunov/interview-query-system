package ru.amprograms.iqs.services;

import java.util.Set;

import ru.amprograms.iqs.services.exceptions.IqsPositionCycle;
import ru.amprograms.iqs.services.exceptions.IqsPositionExists;
import ru.amprograms.iqs.services.exceptions.IqsPositionIsUsed;
import ru.amprograms.iqs.types.IqsPosition;

/**
 * Methods for working with job positions<br>
 * <br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsPositionService {

	/**
	 * @return all job positions
	 */
	Set<IqsPosition> getAllPositions();

	/**
	 * save new job position an return it
	 * 
	 * @param name
	 *            name for the new position
	 * @param parentPosition
	 *            the parent position for the new position or null
	 * @return new job position with ID
	 * @throws IqsPositionExists
	 */
	IqsPosition addPosition(String name, IqsPosition parentPosition) throws IqsPositionExists;

	/**
	 * update existing job position
	 * 
	 * @param position
	 *            the job position
	 * @throws IqsPositionExists
	 */
	void updatePosition(IqsPosition position) throws IqsPositionExists, IqsPositionCycle;

	/**
	 * delete the job position with given ID
	 * 
	 * @param positionId
	 *            the job position ID
	 * @throws IqsPositionIsUsed
	 */
	void deletePosition(Long positionId) throws IqsPositionIsUsed;
}
