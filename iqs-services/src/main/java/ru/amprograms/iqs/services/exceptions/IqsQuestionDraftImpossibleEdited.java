package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * It is impossible to set the draft attribute of the question to true, because
 * the question was edited by other users<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDraftImpossibleEdited extends Exception {

	private static final long serialVersionUID = -6421500870537907738L;
	
	private @Getter Set<IqsUser> users;
}
