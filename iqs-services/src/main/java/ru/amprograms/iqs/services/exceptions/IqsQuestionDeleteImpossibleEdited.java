package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * It is impossible to delete the question, because
 * the question was edited by other users<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDeleteImpossibleEdited extends Exception {

	private static final long serialVersionUID = -8000557127979700916L;
	
	private @Getter Set<IqsUser> users;
}
