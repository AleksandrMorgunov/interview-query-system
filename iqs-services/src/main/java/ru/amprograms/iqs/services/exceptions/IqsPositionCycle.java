package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsPosition;

/**
 * Change the parent job position lead to the cycle of positions
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsPositionCycle extends Exception {

	private @Getter Set<IqsPosition> positionCycle;
}
