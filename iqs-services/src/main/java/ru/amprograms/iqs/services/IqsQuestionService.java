package ru.amprograms.iqs.services;

import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import ru.amprograms.iqs.services.exceptions.IqsQuestionDeleteImpossibleAuthor;
import ru.amprograms.iqs.services.exceptions.IqsQuestionDeleteImpossibleEdited;
import ru.amprograms.iqs.services.exceptions.IqsQuestionDeleteImpossibleQuestionSet;
import ru.amprograms.iqs.services.exceptions.IqsQuestionDraftImpossibleAuthor;
import ru.amprograms.iqs.services.exceptions.IqsQuestionDraftImpossibleEdited;
import ru.amprograms.iqs.services.exceptions.IqsQuestionDraftImpossibleQuestionSet;
import ru.amprograms.iqs.services.exceptions.IqsQuestionExists;
import ru.amprograms.iqs.types.IqsCategory;
import ru.amprograms.iqs.types.IqsItem;
import ru.amprograms.iqs.types.IqsPosition;
import ru.amprograms.iqs.types.IqsQueryCriteria;
import ru.amprograms.iqs.types.IqsQuestion;
import ru.amprograms.iqs.types.IqsQuestionComment;
import ru.amprograms.iqs.types.IqsQuestionMark;
import ru.amprograms.iqs.types.IqsQuestionResourceUrl;
import ru.amprograms.iqs.types.IqsQuestionSet;
import ru.amprograms.iqs.types.IqsQuestionState;
import ru.amprograms.iqs.types.IqsTask;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The methods for working with queries<br>
 * <br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionService {
	/**
	 * Create a new question
	 * 
	 * @param text
	 *            the text of the question
	 * @param answer
	 *            the answer to the question
	 * @param category
	 *            the category of the question
	 * @param positions
	 *            the list of the job positions
	 * @param urls
	 *            the list of the urls related to the question
	 * @param timeForAnswer
	 *            the approximate answer time, minutes
	 * 
	 * @param questionSet
	 *            the set of questions, if it is not null, then new question
	 *            will be added in to it
	 * @param author
	 *            the author of the question
	 * @param isDraft
	 *            is this question a draft
	 * @return the new question
	 */
	IqsQuestion createQuestion(String text, String answer, IqsCategory category, Set<IqsPosition> positions,
			Set<IqsQuestionResourceUrl> urls, Integer timeForAnswer, IqsQuestionSet questionSet, IqsUser author,
			Boolean isDraft) throws IqsQuestionExists;

	/**
	 * Create a new task
	 * 
	 * @param text
	 *            the text of the question
	 * @param answer
	 *            the answer to the question
	 * @param category
	 *            the category of the question
	 * @param positions
	 *            the list of the job positions
	 * @param urls
	 *            the list of the urls related to the question
	 * @param timeForAnswer
	 *            the approximate answer time, minutes
	 * 
	 * @param questionSet
	 *            the set of questions, if it is not null, then new task will be
	 *            added in to it
	 * @param author
	 *            the author of the question
	 * @param isDraft
	 *            is this question a draft
	 * @return the new question
	 */
	IqsTask createTask(String text, String answer, String task, IqsCategory category, Set<IqsPosition> positions,
			Set<IqsQuestionResourceUrl> urls, Integer timeForAnswer, IqsQuestionSet questionSet, IqsUser author,
			Boolean isDraft)
					throws IqsQuestionExists;

	/**
	 * Update question
	 * 
	 * @param item
	 *            the question or task to update
	 * @throws IqsQuestionExists
	 */
	void updateQuestion(IqsItem item) throws IqsQuestionExists, IqsQuestionDraftImpossibleAuthor,
	IqsQuestionDraftImpossibleEdited, IqsQuestionDraftImpossibleQuestionSet;

	/**
	 * Delete the question with given ID
	 * 
	 * @param itemId
	 *            the question or task ID
	 * @throws IqsQuestionDeleteImpossibleAuthor
	 * @throws IqsQuestionDeleteImpossibleEdited
	 * @throws IqsQuestionDeleteImpossibleQuestionSet
	 */
	void deleteQuestion(Long itemId) throws IqsQuestionDeleteImpossibleAuthor, IqsQuestionDeleteImpossibleEdited,
	IqsQuestionDeleteImpossibleQuestionSet;

	/**
	 * 
	 * @param itemId
	 *            the question ID
	 * @return question
	 */
	IqsQuestion getItem(Long itemId);

	/**
	 * Add a mark to the question
	 * 
	 * @param itemId
	 *            the question id
	 * @param value
	 *            the mark value
	 * @param comment
	 *            the comment to the mark or question
	 * @param author
	 *            the user, who rated the question
	 * @return the new mark or updated mark, which has been created by this user
	 *         for this question previously, and the new value for average mark
	 */
	Pair<IqsQuestionMark, Float> addMark(Long itemId, Integer value, String comment, IqsUser author);

	/**
	 * Add a comment to the question
	 * 
	 * @param itemId
	 *            the question ID
	 * @param comment
	 *            the text of the comment
	 * @param author
	 *            the user who created the comment
	 * @return the new comment
	 */
	IqsQuestionComment addComment(Long itemId, String comment, IqsUser author);

	/**
	 * Update the comment
	 * 
	 * @param comment
	 *            the comment to update
	 */
	void updateComment(IqsQuestionComment comment);

	/**
	 * Delete the comment with given ID
	 * 
	 * @param commentId
	 *            the comment ID
	 */
	void deleteComment(Long commentId);

	/**
	 * returns the items matching the specified criteria
	 * 
	 * @param queryCriteria
	 *            the query criteria
	 * @return set of items
	 */
	Set<IqsQuestionState> find(IqsQueryCriteria queryCriteria);
}
