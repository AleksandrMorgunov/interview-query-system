package ru.amprograms.iqs.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsCategory;

/**
 * A category with the same name already exists in the system
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsCategoryExists extends Exception {

	private static final long serialVersionUID = 6355860149628038612L;

	private @Getter IqsCategory currentCategory;
	
	private @Getter IqsCategory existingCategory;
}
