package ru.amprograms.iqs.services.exceptions;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.amprograms.iqs.types.IqsQuestionSet;

/**
 * It is impossible to set the draft attribute of the question to true, because
 * the question is present in another set<br>
 * 
 * @author Alexandr Morgunov
 *
 */
@AllArgsConstructor
public class IqsQuestionDraftImpossibleQuestionSet extends Exception {

	private static final long serialVersionUID = -6644408369593837542L;
	/**
	 * the list of the sets which are included the question
	 */
	private @Getter Set<IqsQuestionSet> questionSets;

}
