package ru.amprograms.iqs.services;

import java.util.Set;

import ru.amprograms.iqs.services.exceptions.IqsCategoryCycle;
import ru.amprograms.iqs.services.exceptions.IqsCategoryExists;
import ru.amprograms.iqs.services.exceptions.IqsCategoryIsUsed;
import ru.amprograms.iqs.types.IqsCategory;

/**
 * The methods for the forking with query category
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsCategoryService {

	Set<IqsCategory> getAllCategories();

	IqsCategory addCategory(String name, IqsCategory parentCategory) throws IqsCategoryExists;

	void updateCategory(IqsCategory category) throws IqsCategoryExists, IqsCategoryCycle;

	void deleteCategory(Long categoryId) throws IqsCategoryIsUsed;
}
