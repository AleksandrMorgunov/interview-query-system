package ru.amprograms.iqs.services;

import java.util.List;
import java.util.Set;

import ru.amprograms.iqs.types.IqsQuestionSet;
import ru.amprograms.iqs.types.IqsQuestionState;
import ru.amprograms.iqs.user.types.IqsUser;

/**
 * The methods for working with a set of questions<br>
 * 
 * @author Alexandr Morgunov
 *
 */
public interface IqsQuestionSetService {

	/**
	 * Create a new set of questions
	 * 
	 * @param name
	 *            the name
	 * @param items
	 *            the ordered list of questions and tasks
	 * @param author
	 *            the author and owner of the set
	 * @param availableToAll
	 *            if this set is available to all users
	 * @return the set of queries and tasks
	 */
	IqsQuestionSet createSet(String name, List<? extends IqsQuestionState> items, IqsUser author,
			Boolean availableToAll);

	/**
	 * Add items (question, task) to the set
	 * 
	 * @param setId
	 *            the set ID
	 * @param itemsIds
	 *            items to add
	 * @param author
	 *            the user, who are adding the item
	 * @return new value time for answer, minutes
	 */
	Integer addItems(Long setId, List<Long> itemsIds, IqsUser author);

	/**
	 * Remove items (question, task) from the set
	 * 
	 * @param setId
	 *            the set ID
	 * @param itemIds
	 *            items to remove
	 * @param author
	 *            the user, who are adding the item
	 * @return new value for time for answer, minutes
	 */
	Integer removeItems(Long setId, List<Long> itemsIds, IqsUser author);

	/**
	 * Return list of items
	 * 
	 * @return the ordered list of questions and tasks
	 */
	List<? extends IqsQuestionState> getItems();

	/**
	 * Return set of question sets available for the given user
	 * 
	 * @param user
	 * @return
	 */
	Set<IqsQuestionSet> getQuestionSets(IqsUser user);

	/**
	 * Return question set with given ID
	 * 
	 * @param questionSetId
	 * @return
	 */
	IqsQuestionSet get(Long questionSetId);

	/**
	 * Update question set
	 * 
	 * @param questionSet
	 *            the question set with new values for the fields, including
	 *            author
	 */
	void update(IqsQuestionSet questionSet);

	/**
	 * Delete the question set with given ID
	 * 
	 * @param questionSetId
	 */
	void delete(Long questionSetId);
}
